//
//  ProfileViewModel.swift
//  Messenger
//
//  Created by Алексей Авдейчик on 29.07.21.
//

import Foundation

enum ProfileViewModelType {
    case info
    case logout
}

struct ProfileViewModel {
    let viewModelType: ProfileViewModelType
    let title: String
    let handler: (() -> Void)?
}
