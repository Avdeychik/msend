//
//  SearchResult.swift
//  Messenger
//
//  Created by Алексей Авдейчик on 29.07.21.
//

import Foundation

struct SearchResult {
    let name: String
    let email: String
}
